#include "types.h"
#include "stat.h"
#include "user.h"

int main(void)
{
    int processNumber = getProcCount();
    printf(1, "\nThe number of processes are %d\n", processNumber);
    exit();
}